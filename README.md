## This file contains Tables S1-S5, the Rmarkdowns corresponding to Files S2-S6 and the corresponding output data tables that were used to do the plots and the statistical analyses.

## Tables S2-S3

#These two tables include sfs, pn/ps and dn/ds per gene and all variables analysed for A.thaliana (S2) and D. melanogaster (S3)

#Column description (S2 and S3):

#sfsN: Non-synonymous SFS
#sfsS: Synonymous SFS
#Pn: number of non-synonymous polymorphisms
#Ps: number of synonymous polymorphisms
#LN: number of non-synonymous sites at divergence
#LS: number of synonymous sites at divergence
#dN: number of non-synonymous substitutions
#dS: number of synonymous substitutions
#Length: protein length
#CatLength: category made for protein length
#Nintrons: number of introns
#CatNintrons: category for number of introns
#protLoc: cellular localization of proteins
#CatLoc: category of protein location
#PPeptides: proportion of peptides binding to dnaK
#CatDnaK: category of the binding affinity to dnaK; 1:non-binder, 2:binder
#KeggClass: protein functional class
#SCat: category of protein functional class
#p.dis: proportion of disordered residues
#pdisCat: category of proportion of disordered residues
#mean.rr: mean value of recombination rate
#CatSplineRR: category of recombination rate  
#nr.AnatStr: number of anatomical structures a gene is expressed (breadth of expression)
#mean.exp: mean gene expression
#BreadthCat: category for the breadth of expression
#MeanExpCat: categories made for the mean gene expression levels

## Exceptions for Table S3:

#dnaKScore: LIMBO score per gene
#degree.ppi: number of protein-protein interactions (PPI)
#CatPPI: category of PPI

# Tables S4-S5

#These two tables include sfs, pn/ps and dn/ds per site and all variables analysed for A.thaliana (S4) and D. melanogaster (S5)

#Column description:

#Site: amino-acid residue site nr in the protein sequence
#HotLoops: probability of residue intrinsic disorder
#SS: secondary structure motif
#RSA: relative solvent accessibility
#PSID: Prosite information, NA values correspond to non-annotated sites
#CatPS: category of active sites; 1:non-active, 2:active
#SSCat: category of SS; 1:helices, 2:beta-sheets, 3:loops
#RSACat: categories made for RSA
#DisCat: categories of residue intrinsic disorder
  
